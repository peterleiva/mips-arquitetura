# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# 		Programa feito para a diciplina de Arquitetura de Computadores do
#   										Professor Luciano Bertini
#
# 									Alunos: Bruno Henrique e Peter Leiva
#
# Descricao: Este programa realiza uma busca linear em um vetor de no maximo
# 100 posicoes, sendo os valores do vetor inseridos pelo usuario, há tambem
# como imprimir o vetor e uma opcao de sair do programa existe duas opções para
# a busca linear, sequencial sem recursão e com recursão. O Programa não checa
# a quantidade máxima de elementos do vetor
#
# Registradores de Entrada e de Saida estão ao longo do corpo do programa.
#
#
#                      			Para Descontrair:
# 		 "When I wrote this, only God and I understood what I was doing.
# 													Now, God only knows!"
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

.data
	# define o vetor usado para armazenar os valores para busca sequencial
	vetor: .space 400

	# constantes globais
	MAX: .word 100 # tamanho máximo do vetor
	OPCAO_FIM: .word 5 # opção de menu para terminar o programa

	# define as mensagens de aviso, menu, e as strings de informacao
	msg0:	.asciiz "Digite a quantidade de itens que serao inseridos: "
	menu:	.asciiz "Menu:\n[1] entrada\n[2] pesquisar\n[3] imprimir itens no vetor\n[4] Busca Recursiva\n[5] Fim\n\n"
	msg5:	.asciiz "Digite o novo numero a ser inserido: "
	msg6:	.asciiz "Digite o numero a ser pesquisado: "
	msg7:	.asciiz "elemento encontrado na posicao "
	msg8:	.asciiz "Elemento nao encontrado"
	saida:	.asciiz "\nPrograma terminado\n"
	opcao_invalida:	.asciiz "Opcao invalida\n"
	msg11:	.asciiz "elemento na posicao "
	separador: .asciiz "\n\n----------------------------------------------\n\n"
	opcao: .asciiz "opcao: "

.text

.globl main

##
# Método principal do programa
#
# Imprime as opções que o programa realiza efetuando um loop que é executado
# para cada escolha de funcionalidade requerida pelo usuário. Há um switch
# no loop que verifica as opções e funcionalidades. Os registradores $s0
# armazena a opção de menu escolhido pelo usuário, $s1 o tamanho do vetor
# usado para a busca sequencial
#
##
main:

	#subtrai 12 bytes do vetor stack pointer
	addi $sp, $sp, -12

	#guarda o valor de $ra na posição atual de $sp
	sw $ra, 0($sp)

	# inicializa e armazena o tamanho do vetor em $s1
	li $s1, 0
	sw $s1, 8($sp)


	# loop principal que imprime menu e efetua as operações requiridas pelo usuário
	menu_loop:

		# imprime separador entre menus
		li $v0, 4
		la $a0, separador
		syscall

		# imprime menu e funcionalidades do programa
		li $v0, 4
		la $a0, menu
		syscall

		# lê opção de menu escolhida pelo usuario
		la $a0, opcao
		jal ler_inteiro

		# armazena opção escolhida na pilha e em $s0
		move $s0, $v0
		sw $s0, 4($sp)

		# carrega o tamanho do vetor
		lw $s1, 8($sp)

	  ## # # # # #
		# inicio do bloco que verifica qual opcao foi escolhida
		## # # # # # #

		# compara a opção escolhida pelo usuário em sua respectiva função
		li $t0, 1
		beq $t0, $s0, case1_preenche_vetor

		li $t0, 2
		beq $t0, $s0, case2_pesquisa_iterativa

		li $t0, 3
		beq $t0, $s0, case3_imprimir_vetor

		li $t0, 4
		beq $t0, $s0, case4_pesquisa_recursiva

		li $t0, 5
		beq $t0, $s0, fim_bloco_switch

		j default


		# lê a quantidade de inteiros e os respectivos valores do vetor
		case1_preenche_vetor:

			# lê o tamanho do vetor em $s1
			la $a0, msg0
			jal ler_inteiro
			move $s1, $v0

			# armazena o tamanho na pilha
			sw $s1, 8($sp)

			# preenche um vetor com N, $s1, inteiros
			la $a0, vetor
			move $a1, $s1
			jal preenche_vetor

			# pula para o fim do bloco de escolha
			j fim_bloco_switch

		# pesquisa um valor efetuando algoritmo de busca linear sem recursão
		case2_pesquisa_iterativa:

			# lê valor a ser pesquisado
			la $a0, msg6
			jal ler_inteiro

			# carrega os parâmetros da função de pesquisa, o vetor e o seu tamanho
			la $a0, vetor
			move $a1, $s1
			move $a2, $v0

			#chama a função pesquisa_sequencial
			jal pesquisa_sequencial

			# imprime mensagem sobre objeto pesquisado
			#se $v0 = $0 então vá apra else_case6, se não imprima a String contida em msg7
			beq $v0, $0, else_case2
				li $v0, 4
				la $a0, msg7
				syscall

			# Imprime o valor contido em $v1 e pula para o fim do bloco de escolha
				li $v0, 1
				move $a0, $v1
				syscall
				j fim_bloco_switch

			# Imprime a String contida em msg8
			else_case2:
				li $v0, 4
				la $a0, msg8
				syscall
				j fim_bloco_switch

		# Imprime um vetor preenchido pelo usuário
		case3_imprimir_vetor:

			# define os argumentos de imprimir_vetor
			la $a0, vetor
			move $a1, $s1
			jal imprimir_vetor

			j fim_bloco_switch

		# pesquisa um valor efetuando algoritmo de busca linear com recursão; rever
		case4_pesquisa_recursiva:

			# lê um inteiro
			la $a0, msg6
			jal ler_inteiro

			# define os argumentos de pesquisa_sequencial

			# armazena endereço de vetor em $a0
			la $a0, vetor

			# carrega o tamanho do vetor em $a1
			move $a1, $s1

			# carrega valor a ser pesquisado em $a2 retornado em $v0 por ler_inteiro
			move $a2, $v0

			# chama pesquisa sequencial(vetor, N, item)
			jal pesquisa_sequencial

			# imprime mensagem sobre objeto pesquisado
			beq $v0, $0, else_case4
				li $v0, 4
				la $a0, msg7
				syscall

				li $v0, 1
				move $a0, $v1
				syscall
				j fim_bloco_switch
			else_case4:
				li $v0, 4
				la $a0, msg8
				syscall
				j fim_bloco_switch

		# opção default, informa que a opção é invalida ao usuário
		default:

			li $v0, 4
			la $a0, opcao_invalida
			syscall


		## # # # # # #
		# fim do bloco de verificacao de escolha
		## # # # # # #

		fim_bloco_switch:

			# carrega a opção fim de menu na pilha em $t0
			la $t0, OPCAO_FIM
			lw $t0, 0($t0)

			# carrega opção escolhida da pilha que pode ser alterada por chamadas
			lw $s0, 4($sp)

			# compara a opção escolhida com as funcionalidades requeridas
			bne $s0, $t0, menu_loop

		saida_principal:

		# volta o estado original
		lw $ra, 0($sp)
		addiu $sp, $sp, 12

		jr $ra

##
# Realiza uma busca linear iterativa em um vetor de inteiro
#
# Argumentos $a0 Endereço base de vetor
#						 $a1 Tamanho do vetor
#						 $a2 Item a ser pesquisado
#
# Retorno   $v0 1, Verdade caso ache, ou 0, falso caso não ache o valor
##
busca_linear:
	addiu $sp, $sp, -28

	# inicialize i($s0), $v0 e $v1
	move $s0, $0
	move $v0, $0
	li $v1, -1

	sw $v1, 24($sp)
	sw $v0, 20($sp)
	sw $a2, 16($sp)
	sw $a1, 12($sp)
	sw $a0, 8($sp)
	sw $s0, 4($sp)
	sw $ra, 0($sp)

	# loop while que testa sequencialmente cada elemento do vetor
	loop_busca_linear:
		bge $s0, $a1, saida_busca_linear # i >= N termina o loop

		# lê o valor em vetor[i] e armazena em $t0
		move $a1, $s0
		jal get
		move $t0, $v0

		lw $a2, 16($sp)
		lw $a1, 12($sp)
		lw $a0, 8($sp)
		lw $s0, 4($sp)

		# verifica se vetor[i] != item
		bne $t0, $a2, nao_encontrado

		# define os valores de retorno no caso de sucesso
		li $v0, 1
		move $v1, $s0

		sw $v0, 16($sp)
		sw $v1, 12($sp)

		j saida_busca_linear

		# executa o else do teste vetor[i] == item
		nao_encontrado:

			# incrementa i; i++
			addiu $s0, $s0, 1

			# armazena i na pilha
			sw $s0, 4($sp)

			j loop_busca_linear

	saida_busca_linear:

		lw $ra, 0($sp)
		lw $v0, 16($sp)
		lw $v1, 20($sp)
		addiu $sp, $sp, 28

		jr $ra

##
# Preenche um vetor com um conjunto de inteiros de usuário
#
#
# Argumentos	$a0	Endereço de vetor
# 						$a1 Tamanho do vetor
#
# Retorno 		void
##
preenche_vetor:

	addi $sp, $sp, -20
	move $s0, $zero
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $s0, 12($sp)

	preenche_loop:

		# se $s3 = $t1 pula para a funcao exit
		bge	$s0, $a1, saida_preenche	# if i >= N, pula para saida_preenche

		# lê um novo valor e armazena em $s0
		la $a0, msg5
		jal ler_inteiro

		# recupera endereço de vetor
		lw $a0, 4($sp)

		# define valor de vetor[i] como valor passado pelo usuário
		move $a1, $s0
		move $a2, $v0

		# set(vetor, i, valor)
		jal set

		lw $a1, 8($sp)
		lw $s0, 12($sp)

		#incrementa o contador $t1
		addiu 	$s0, $s0, 1	# i = i + 1

		sw $s0, 12($sp)

		j preenche_loop

	saida_preenche:
		lw $ra, 0($sp)
		add $sp, $sp, 20
		jr $ra

##
# Lê um inteiro de um vetor de um dado índice
#
# Efetua leitura de um inteiro na memória para isso é calculado o seu endereço
# base, onde cada inteiro ocupa uma palavra de memória. O valor lido é retornado
# em $v0
#
# argumentos	$a0(vetor) Endereço do vetor
# 						$a1(i)		 índice do vetor
#
# retorna 		$v0 Inteiro em vetor[i]
##
get:

	# calcula endereço base de vetor
	sll $t0, $a1, 2 # $t0 = i * 4
	add $t0, $a0, $t0

	lw $t0, 0($t0) # $t0 = vetor[i]
	move $v0, $t0

	jr $ra

##
# Preenche um inteiro de um vetor de um dado índice
#
# Efetua o armazenamento de um inteiro na memória para isso é calculado o seu
# endereço base, onde cada inteiro ocupa uma palavra de memória. O valor lido
# e retornado em $v0
#
# argumentos	$a0(vetor) Endereço do vetor
# 						$a1(i)		 índice do vetor
# 						$a2(valor) valor definido em vetor[i]
#
# retorna 		void
##
set:
 	sll $t0, $a1, 2
	add $t0, $a0, $t0

	sw $a2, 0($t0)

	jr $ra

##
# Lê um inteiro e imprime uma mensagem
#
# Exibe uma mensagem passada como argumento em $a0 e lê um inteiro. O inteiro
# lido é retornado em $v0
#
# Parametros  $a0 Mensagem que a ser impressa
#
# Retorno 		$v0 Inteiro lido
##
ler_inteiro:

	# imprime a mensagem passada em $a0
	li $v0, 4
	syscall

	# Lê um inteiro do I/O
	li $v0, 5
	syscall

	jr $ra

##
# Imprime um vetor de inteiros
#
# Imprime um vetor de tamanho $a1, N, sendo seu endereço passado em $a0
#
# argumentos $a0(vetor) Endereço do vetor a ser impresso
# 					 $a1(N) 		Tamanho do vetor
#
# retorno 	 void
##
imprimir_vetor:

	# definição da pilha
	addi $sp, $sp, -16

	# inicializa i = 0
	move $s0, $0

	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $s0, 12($sp)

	# loop for que imprime o vetor de tamanho N
	imprime:

		# se i >= N então pule para saida_imprimir
		bge $s0, $a1, saida_imprimir

		# lê o inteiro em vetor[i] e armazena em $t0
		move $a1, $s0
		jal get
		move $t0, $v0

		# imprime o valor da posicao vetor[i]
		li 	$v0, 1
		move 	$a0, $t0
		syscall

		# imprime separador de cada elemento do vetor
		li 	$v0, 11
		li 	$a0, ','
		syscall

		li 	$v0, 11
		li 	$a0, ' '
		syscall

		# recupera dados que podem ser usados por chamada de função
		lw $a0, 4($sp)
		lw $a1, 8($sp)
		lw $s0, 12($sp)

		# incrementa o contador
		addi 	$s0, $s0, 1	# i = i + 1

		# armazena i, $s0, incrementado na pilha
		sw $s0, 12($sp)

		# continua o loop de impressão de vetor
		j imprime

	saida_imprimir:

		# recupera os elementos salvos na pilha e retorna
		lw $ra, 0($sp)
		addiu $sp, $sp, 16
		jr $ra

##
# Realiza uma busca recursiva em um vetor de inteiros
#
# Efetua uma busca linear recursiva em uma lista de inteiro, passado pelo seu
# endereçø em $a0. A função usa uma outra função auxiliar,
# pesquisa_sequencial_recursiva, que efetivamente roda o algoritmo. Para seu
# funcionamento é passado o índice inicial da lista em $a3.
# Nota-se que $v0, $v1 não é preciso ser definido na função pois o seu valor é usado
# da função auxiliar. O mesmo vale para $a0, $a1 e $a2
#
# Argumentos: $a0 Endereço do vetor usado para busca
# 						$a1 Tamanho do vetor
# 						$a2 Valor a ser pesquisado
#
# Retorna: $v0 Retorna 1 caso seja encontrado o valor e 0 caso o contrário
# 				 $v1 Posição em que foi encontrado o item ou -1 caso não foi achado
#
##
pesquisa_sequencial:

	# define uma pilha e armazena valores que podem ser usados em jal
	addi $sp, $sp, -16
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $a2, 12($sp)

	move $a3, $0 # i = 0

	# chamada de pesquisa_sequencial_recursiva(vetor, tamanho, item, i = 0)
	jal pesquisa_sequencial_recursiva

	# retorna os valores da pilha
	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $a2, 12($sp)
	addi $sp, $sp, 16

	jr $ra

##
# Realiza uma busca recursiva em um vetor de inteiros
#
# Efetua uma busca linear recursiva em um vetor passado como endereço em $a0
# Para isso o vetor é tratado como um elemento e uma sublista. O algoritmo
# no caso base verifica se a sublista é vazia para isso basta checar se
# se o índice(i) é maior que o tamanho do vetor(N), $a3 >= $a1 (i >= N), nesse
# caso retorna 0.
# O passo indutivo testa se o item atual da lista é o valor buscado, calculando
# o endereço base do valor pesquisado. Em caso positivo retorna verdade
# caso contrário busca em sua sublista, chamando a si mesmo recursivamente com
# i + 1 ($a3 + 1).
#
# Obs: Nota-se que apenas $a3, $ra são salvos na pilha pois são os únicos
# valores de interesse que podem ser alterados pela chamada recursiva. Os valores
# de retorno são definidos recursivamente em $v0 e $v1.
#
# Argumentos: $a0(vetor) 	Endereço vetor no qual será usado pela busca
# 						$a1(N) 			Tamanho do vetor
# 						$a2(item)		Valor a ser pesquisado
#							$a3(i)			Índice do valor pesquisado
#
# Retorna:  $v0 Retorna 1 caso seja encontrado o valor e 0 caso o contrário
# 					$v1 índice do valor retornado, caso exista e -1 caso não exista
#
##
pesquisa_sequencial_recursiva:

	# caso base sublista está vazia
	blt $a3, $a1, passo_indutivo

	# define o valor de retorno como 0 (falso) e retorna
	li $v0, 0
	li $v1, -1
	jr $ra

	# passo indutivo índice não alcançou o limite do vetor
	passo_indutivo:
		addi $sp, $sp, -16
		sw $ra, 0($sp)
		sw $a0, 4($sp)
		sw $a1, 8($sp)
		sw $a3, 12($sp)

		# lê vetor[i] e armazena em $t0
		move $a1, $a3
		jal get
		move $t0, $v0

		lw $a0, 4($sp)
		lw $a1, 8($sp)

		# vetor[i] != item
		bne $t0, $a2, NotFounded

		# elemento encontrado, vetor[i] == item. retorna a função
		li $v0, 1 # $v0 = 1
		move $v1, $a3 # $v1 = i
		j exit

		# Elemento atual, vetor[i] !== item, não foi encontrado
		NotFounded:
			# chamada recursiva
			addiu $a3, $a3, 1 # i = i + 1
			jal pesquisa_sequencial_recursiva
			lw $a3, 12($sp)

		# recupera a pilha e retorna
		exit:
			lw $ra, 0($sp)
			addiu $sp, $sp, 16

			jr $ra
